package main

import (
	"bufio"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"github.com/grandcat/zeroconf"
)

// This could be a HTTP/TCP service or whatever you want.
func startService(c_listen_port chan int) {
	listener, err := net.Listen("tcp", ":0") // next free port
	if err != nil {
		panic(err)
	}

	// give back the listen port
	port := listener.Addr().(*net.TCPAddr).Port
	c_listen_port <- port

	conn, err := listener.Accept()
	if err != nil {
		panic(err)
	}

	for {
		message, _ := bufio.NewReader(conn).ReadString('\n')
		log.Println("Message Received:", message)
	}
}

func cleanup(service *zeroconf.Server) {
	service.Shutdown()
	os.Exit(1)
}

func server() {
	// Start out http service
	c_listen_port := make(chan int) // for getting listen port
	go startService(c_listen_port)

	// Extra information about our service
	meta := []string{
		"version=0.0.1",
		"author=manu",
	}

	var port int
	port = <-c_listen_port
	close(c_listen_port)
	log.Println("listening on", port)
	service, err := zeroconf.Register(
		"mine",         // service instance name
		"_carmen._tcp", // service type and protocl
		"local.",       // service domain
		port,           // service port
		meta,           // service metadata
		nil,            // register on all network interfaces
	)

	if err != nil {
		log.Fatal(err)
	}

	defer cleanup(service)

	// After setting everything up!
	// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
	signalChan := make(chan os.Signal)
	cleanupDone := make(chan struct{})
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)
	go func(service *zeroconf.Server) {
		<-signalChan
		log.Println("Received an interrupt, stopping zeroconf service...")
		cleanup(service)
		var s struct{}
		cleanupDone <- s
	}(service)
	<-cleanupDone
}
