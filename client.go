package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"strconv"
	"sync"
	"time"

	"github.com/grandcat/zeroconf"
)

func client() {
	log.Println("client")
	resolver, err := zeroconf.NewResolver(nil)
	if err != nil {
		log.Fatalln("Failed to initialize resolver: ", err.Error())
	}

	var wg sync.WaitGroup
	wg.Add(1)
	entries := make(chan *zeroconf.ServiceEntry)
	go func(results <-chan *zeroconf.ServiceEntry) {
		defer wg.Done()
		for entry := range results {
			log.Println("=========")
			log.Println(entry.HostName)
			log.Println(entry.Port)
			log.Println(entry.AddrIPv4)
			log.Println(entry.Text)
			addr := entry.AddrIPv4[0].String() + ":" + strconv.Itoa(entry.Port)
			conn, err := net.Dial("tcp", addr)
			if err != nil {
				panic(err)
			}
			fmt.Print("sending hello\n")
			fmt.Fprintf(conn, "hello\n")
		}
		log.Println("Exhausted service list.")
	}(entries)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(10))
	defer cancel()
	// Look for our service, and send to the goroutine
	err = resolver.Browse(ctx, "_carmen._tcp", "local", entries)
	if err != nil {
		log.Fatalln("Failed to browse: ", err.Error())
	}

	<-ctx.Done()

	// wait for goroutine to finish
	wg.Wait()
}
