package main

import (
	"flag"
	"log"
)

func main() {
	clientPtr := flag.Bool("client", false, "client mode")
	serverPtr := flag.Bool("server", false, "client mode")

	flag.Parse()

	if *clientPtr == *serverPtr {
		log.Fatalln("specify either -client or -server")
	}

	if *clientPtr {
		client()
	} else {
		server()
	}
}
